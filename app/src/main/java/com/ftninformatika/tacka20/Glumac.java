package com.ftninformatika.tacka20;

import java.util.Date;

public class Glumac {

    private String ime;
    private String prezime;
    private String slika;
    private String biografija;
    private Date datum;
    private double ocena;
    private String filmovi;

    public Glumac(String ime, String prezime, String slika, String biografija, Date datum, double ocena, String filmovi) {
        this.ime = ime;
        this.prezime = prezime;
        this.slika = slika;
        this.biografija = biografija;
        this.datum = datum;
        this.ocena = ocena;
        this.filmovi = filmovi;
    }

    public String getIme() {
        return ime;
    }

    public void setIme(String ime) {
        this.ime = ime;
    }

    public String getPrezime() {
        return prezime;
    }

    public void setPrezime(String prezime) {
        this.prezime = prezime;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public String getBiografija() {
        return biografija;
    }

    public void setBiografija(String biografija) {
        this.biografija = biografija;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }


    public double getOcena() {
        return ocena;
    }

    public void setOcena(double ocena) {
        this.ocena = ocena;
    }

    public String getFilmovi() {
        return filmovi;
    }

    public void setFilmovi(String filmovi) {
        this.filmovi = filmovi;
    }

    @Override
    public String toString() {
        super.toString();
        return ""
    }
}
