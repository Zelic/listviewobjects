package com.ftninformatika.tacka20;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class FragmentLista extends Fragment {

    private OnGlumacClickedListener listener = null;
    ListView listView;
    //Activity activity;
    private List<Glumac> novaLista = new ArrayList<>();





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_lista, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onAttach(Context activity) {
        super.onAttach(activity);
        try {
            listener = (OnGlumacClickedListener) activity;
        } catch (ClassCastException e) {
            Toast.makeText(getActivity(),"Nema listnera tj kasta",Toast.LENGTH_LONG).show();
        }
    }
    private void setupList(){
        listView = getView().findViewById(R.id.frame);
        listView.setAdapter(new MojListAdapter(GlumacProvider.glumci()));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(listener!=null){
                    listener.onGlumacClicked(GlumacProvider.glumci().get(position));
                }else{
                    Toast.makeText(getActivity(),"Kliknuto",Toast.LENGTH_LONG).show();

                }
            }
        });

            }
}