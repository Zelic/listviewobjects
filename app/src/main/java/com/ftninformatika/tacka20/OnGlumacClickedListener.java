package com.ftninformatika.tacka20;

import java.util.List;

public interface OnGlumacClickedListener {
    void onGlumacClicked();
}
