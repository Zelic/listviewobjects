package com.ftninformatika.tacka20;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class MojListAdapter extends BaseAdapter {
    //Activity activity;
    List<Glumac> lista;


    public MojListAdapter( List<Glumac> lista) {
        //this.activity = activity;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView == null)
            convertView = LayoutInflater.from(convertView.getContext()).inflate(R.layout.postavka_imena,null);

        TextView tvIme = convertView.findViewById(R.id.ime_prezime);
        tvIme.setText(lista.get(position).getIme()+ " " +lista.get(position).getPrezime());
        return convertView;
    }
}
