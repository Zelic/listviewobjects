package com.ftninformatika.tacka20;

import android.opengl.GLUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class GlumacProvider {



    public static List<Glumac> glumci(){
        List<Glumac> listaGlumaca = new ArrayList<>();
        listaGlumaca.add(new Glumac("Milan","Gutovic","","33 filma",new Date(1952-3-22),5.8,"Tesna koza"));
        listaGlumaca.add(new Glumac("Srdjan","Todorovic","","56 filma",new Date(1968-6-13),7.4,"Tri palme"));
        listaGlumaca.add(new Glumac("Dragan","Bjelogrlic","","67 filma",new Date(1965-12-10),6.2,"Lepa"));
        listaGlumaca.add(new Glumac("Nikola","Kojo","","71 filma",new Date(1966-8-8),8.1,"Mi nismo andjeli"));
        return listaGlumaca;
    }



}
