package com.ftninformatika.tacka20;

import android.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ListView;

import java.util.List;

public class MainActivity extends AppCompatActivity implements OnGlumacClickedListener {


    Toolbar toolbar;
    DrawerLayout drawerLayout;
    ListView drawerList;
    ActionBarDrawerToggle drawerToggle;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        showList();
        setupToolbar();
        setupDrawer();

    }

    private void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final android.support.v7.app.ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);//klik da se vrati na perenta
            actionBar.setHomeAsUpIndicator(R.drawable.ic_action_name);//hamburger ikonica
            actionBar.setHomeButtonEnabled(true);
            actionBar.show();
        }
    }
    private void setupDrawer(){
        drawerList = findViewById(R.id.left_drawer);
        drawerLayout = findViewById(R.id.drawer_layout);

                drawerLayout.closeDrawer(drawerList);





        drawerToggle = new ActionBarDrawerToggle(
                this,
                drawerLayout,
                toolbar,
                R.string.app_name,
                R.string.app_name
        ) {
            public void onDrawerClosed(View view) {

                invalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }
        };
    }
    public void showList(){
        FragmentLista fragment = new FragmentLista();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.root, fragment);
        transaction.commit();
    }

    @Override
    public void onGlumacClicked() {
        FragmentDetalji fragment = new FragmentDetalji();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.root,fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}